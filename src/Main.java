
import java.io.File;

import java.io.IOException;
import java.util.Scanner;



public abstract class Main{
	
	public static void menu() throws InterruptedException, IOException{
		Scanner sc = new Scanner(System.in);
		 String sfile;
         
		System.out.println("Introduce el fichero con las url que quieres descargar");
		String readFile = sc.next();
		sfile = readFile;
		File file = new File(sfile);
		while(!file.exists()){
			System.err.println("Fichero de lectura inexistente");
			file.delete();
			System.out.println("Introduce el fichero con las url que quieres descargar");
			readFile = sc.next();
			file = new File(readFile);
			
		}
			
		System.out.println("Introduce el nombre del directorio donde quieres guardar las descargas");
		String writeFile = sc.next();
		System.out.println("Introduce el numero de procesos de descarga que deseas crear");
		int nDown = Integer.parseInt(sc.next());
		System.out.println("Introduce el numero de procesos simultaneos");
		int maxDown = Integer.parseInt(sc.next());
		if(nDown<maxDown){
			System.err.println("Error, el numeros de procesos creados es menos que los procesos simultaneos");
		}else{
		
			WebProcessor wp = new WebProcessor(writeFile,nDown,maxDown);
			wp.process(readFile);
			sc.close();
		}
		
	}
	
	
	
	public static void main(String[] args) throws InterruptedException, IOException {
	
		menu();//arrancado provisional


	}

}
