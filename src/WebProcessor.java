import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;

public class WebProcessor {

	private int counter = 0;
	private int nThreads = 0;

	private String writePath;
	private String readPath;

	private Reader r;
	private BufferedReader br;

	private File dir;

	private Writer w;
	private PrintWriter pw;
	private boolean eof;

	private Writer werror;
	private PrintWriter error;

	private Semaphore semExc;
	private Semaphore lectura;

	private List<Thread> array = new ArrayList<Thread>();
	private Thread key;

	public WebProcessor(String wpath, int nDown, int maxDown)throws IOException {

		// inicializo varibales, rutas de escritura y lectura y creo el
		// directorio de escritura
		this.nThreads = nDown;
		this.writePath = wpath;
		dir = new File(getWritePath()+"/error");
		dir.mkdir();
		werror = new FileWriter(getWritePath()+ "/error/error_log.txt");
		error = new PrintWriter(werror);
		this.eof=false;
		

		// inicializo semaforos
		semExc = new Semaphore(maxDown);
		lectura = new Semaphore(1);

	}

	public void process(String fileName) throws InterruptedException, FileNotFoundException {

		setReadPath(fileName);
		initReader(getReadPath());
		// Threads
		for (int i = 0; i < this.nThreads; i++) {
			array.add(new Thread(() -> start(), "hilo" + i));
		}
		// Contador
		Thread timer = new Thread(() -> showInfo(), "Timer");
		key = new Thread(()-> listener(),"Key");
		for (Thread th : array) {
			th.start();
		}
		timer.start();
		key.start();

		for (Thread th : array) {
			th.join();
		}
		timer.join();
		key.join();

	}

	public void start() {

		try {
			String str;
			lectura.acquire();//Seccion critica
			while ((str = br.readLine()) != null) {
				
				lectura.release();//Fin de seccion critica
				semExc.acquire();//Threads descargando simultaneamente
				
				download(str);
				
				semExc.release();
				if(!key.isAlive()){//Si se ha pulsado enter acabo con todos los hilos					
					break;
					
				}

			}
			
			closeReader();
			closeError();
			this.eof=true;
		} catch (IOException | InterruptedException e) {
			// e.printStackTrace();

		}


	}

	public void download(String url) throws InterruptedException{

		// Creates a connection to a given url
		Connection conn = Jsoup.connect(url);

		try {
			// Performs the connection and retrieves the response
			Response resp = conn.execute();
			// If the response is different from 200 OK,
			// the website is not reachable
			if (resp.statusCode() != 200) {

				System.out.println("Error: " + resp.statusCode());
				writeError(url);

			} else {
				//System.out.println("Todo bien");
				
				
				String html = conn.get().html();
				writeFile(html,url);


			}
		} catch (IOException e) {
			//System.out.println("No se puede conectar" + url);
			writeError(url);
		}
		this.counter++;

	}


	private void writeError(String url) {

		error.println(url);
		error.flush();

	}

	// consigo el nombre del fichero
	private String nameFile(String url) {
		String[] words = url.split("/");
		return words[words.length - 1] + ".html";
	}

	// inicializar fichero lectura
	private void initReader(String rPath) throws FileNotFoundException {
		r = new FileReader(rPath);
		br = new BufferedReader(r);

	}

	// escribo html en fichero
	private void writeFile(String html, String url) throws IOException {
		w = new FileWriter(getWritePath() +"/"+ nameFile(url));
		pw = new PrintWriter(w);
		pw.println(html);
		pw.flush();
		if (w != null) {
			w.close();
			pw.close();
		}

	}

	
	private void closeReader() throws IOException{
		if(r!=null){
			r.close();
			pw.close();
		}
	}
	private void closeError() throws IOException{
		if(werror!=null){
			werror.close();
			error.close();
		}
	}

	private void showInfo() {
		while (!this.eof) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("El numero de urls procesadas es de: "
					+ this.counter);
			//System.out.println("Procesando, por favor espere...");
		}
		

	}
	
	private void listener(){
		InputStream is = System.in;
		try {
			while(is.available()==0){
				
				Thread.sleep(500);
				if(this.eof){
					Thread.interrupted();
					break;
				}
			
			}
		
			System.out.println("Deteniendo procesos de descarga...");
			Thread.interrupted();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}	

	public String getWritePath() {
		return writePath;
	}

	public void setWritePath(String writePath) {
		this.writePath = writePath;
	}

	public String getReadPath() {
		return readPath;
	}

	public void setReadPath(String readPath) {
		this.readPath = readPath;
	}


}
